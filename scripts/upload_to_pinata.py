import requests
import os
from PIL import Image
import numpy as np
import cv2
import json


class PinataUploader():
    def __init__(self, filename='image.png', jsonfile = 'panda.json', pinata_key=None, pinata_secret=None):
        # Default filename in case no filename is provided later on.
        self.filename = filename
        self.jsonfile = jsonfile

        # Pinata access information.
        self.PINATA_BASE_URL = 'https://api.pinata.cloud/'
        self.endpoint = 'pinning/pinFileToIPFS'


    def upload_image(self, image, filename=None, pinata_key_ending=''):
        # Header containing Pinata credentials.
        headers = {
            'pinata_api_key': os.getenv('PINATA_API_KEY' + pinata_key_ending),
            'pinata_secret_api_key': os.getenv('PINATA_API_SECRET' + pinata_key_ending)
        }

        # Encodes the image as byte sequence.
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGRA)
        _, image_encoded = cv2.imencode('.png', image)

        if filename is None:
            filename = self.filename

        response = requests.post(
            self.PINATA_BASE_URL + self.endpoint,
            files={"file": (filename, image_encoded)},
            headers=headers
        )

        return response.json()['IpfsHash']

    def upload_json(self, meta, jsonfile=None, pinata_key_ending=''):

        headers = {
            'pinata_api_key': os.getenv('PINATA_API_KEY' + pinata_key_ending),
            'pinata_secret_api_key': os.getenv('PINATA_API_SECRET' + pinata_key_ending)
        }

        if jsonfile is None:
            jsonfile = self.jsonfile

        string = json.dumps(meta)
        #binary = ' '.join(format(ord(letter), 'b') for letter in string)

        response = requests.post(
            self.PINATA_BASE_URL + self.endpoint,
            files={"file": (jsonfile, string)},
            headers=headers
        )

        return response.json()['IpfsHash']

    def upload_directory(self, filePath, pinata_key_ending=''):

        headers = {
            'pinata_api_key': os.getenv('PINATA_API_KEY' + pinata_key_ending),
            'pinata_secret_api_key': os.getenv('PINATA_API_SECRET' + pinata_key_ending)
        }

        files = []
        filenames = os.listdir(filePath)
        filenames.sort(key= lambda x: float(x))
        for idx, i in enumerate(filenames):
            filename = os.path.join('metadata_last_test', str(idx+1))
            with open(os.path.join(filePath, i), 'rb') as fb:
                content = fb.read()
            files.append(('file', (filename, content)))
        print('send')
        response = requests.post(
        self.PINATA_BASE_URL + self.endpoint,
        files=files,
        headers=headers
        )
        print(response.json())





if __name__== '__main__':
    filepath = 'JSON-FILES/'

    pinata_uploader = PinataUploader()
    pinata_uploader.upload_directory(filepath)
    #print("You can find the image at https://ipfs.io/ipfs/" + ipfs_hash)

import numpy as np
from PIL import Image
import random
import os
from upload_to_pinata import PinataUploader
import pandas as pd
import json
import metadata_basic
import sys

def generateTrait():
    trait = {}
    for idx, key in enumerate(parts_dict):
        trait[key] = rng[idx].choice(parts_dict[key], 1, p = probability_dict[key])[0]
    if trait in traits:
        trait = generateTrait()
        return trait
    else:
        return trait

def getDictFromExcel(names, probabilities):
    dict_proba = {}
    for i in range(len(names)):
        if pd.isna(names[i]):
            break
        dict_proba[names[i]] = probabilities[i]
    return dict_proba


def getPartsDict(folderPath):
    parts_dict = {}
    probability_dict = {}

    excelFile = pd.read_excel(folderPath + 'traits.xlsx', engine='openpyxl')
    #print(excelFile)

    for root, folder, files in os.walk(folderPath):
        basename = os.path.basename(root)
        if basename == '':
            continue
        parts_dict[basename] = [File for File in files if not File.startswith('.')]
        dict_proba = getDictFromExcel(excelFile[basename], excelFile[basename + '_Proba'])
        probability_dict[basename] = [dict_proba[name[:-4]] for name in parts_dict[basename]]

    return parts_dict, probability_dict

def allUnique(x):
    seen = list()
    return not any(i in seen or seen.append(i) for i in x)

def addTokenID(traits):
    for i, item in enumerate(traits):
        item['tokenID'] = i+1
    return traits

def generateImage(imgDict, keys, folderPath):

    filename_background = folderPath + keys[0] + '/' + imgDict[keys[0]]
    im1= Image.open(filename_background).convert('RGBA')
    #im1 = Image.new("RGBA", im_back.size)
    #im1.paste(im_back, (0, 0), im_back)
    for key in keys[1:-1]:
        filename = folderPath + key + '/' + imgDict[key]
        im2 = Image.open(filename).convert('RGBA')
        #if key in ['Shadow']:
        #    im1 = Image.blend(im1, im2, alpha = 0.1)
        #else:
        im1.paste(im2, (0,0), im2) 
        #im1 = Image.alpha_composite(im1, im2) #im1.paste(im2, (0,0), im2), 
        im2.close()
    # im1 = im1.resize((400, 400), resample=Image.BOX)
    savepath = '../Funghis/'
    filename = savepath + 'funghi_' + str(imgDict[keys[-1]]) + '.png'
    #im1.convert('RGB')
    im1.save(filename)
    img = np.asarray(im1)
    im1.close()
    return img, filename.split('/')[2]


def traitStatistics(traits, trait_keys, parts_dict):
    statistics = {}

    for key in trait_keys:
        key_stats = {}
        for part in parts_dict[key]:
            intern_statistics = np.sum([1 for trait in traits if trait[key] == part])
            key_stats[part] = intern_statistics / len(traits)
        statistics[key] = key_stats

    with open('statistics.json', 'w') as fp:
        json.dump(statistics, fp)

    return statistics

def saveMetadata(trait, trait_keys):
    trait_metadata = metadata_basic.sample_metadata
    trait_metadata['name'] = "Funghi #" + str(trait['tokenID'])
    trait_metadata['image'] = "https://ipfs.io/ipfs/" + trait['ipfs_hash']

    for i, key in enumerate(trait_keys[:-1]):
        if trait_metadata['attributes'][i]['trait_type'] == key:
            trait_name = trait[key][:-4]
            trait_name.replace('_', ' ')
            trait_metadata['attributes'][i]['value'] = trait_name
        else:
            print("Error: Trait-Type and Key mismatch. No metadata created!")
            return None
    filename = "moonfighters_meta_" + str(trait['tokenID']) + '.json'
    savepath = '../Masks/JSON-FILES'
    with open(os.path.join(savepath, str(trait['tokenID'])), 'w') as fp:
        json.dump(trait_metadata, fp)
    #ipfs_hash = pinata_uploader.upload_json(trait_metadata, filename)
    #return ipfs_hash

if __name__ == '__main__':
    os.makedirs('../Funghi/JSON-FILES', exist_ok=True)
    folderPath = "../layers/"
    parts_dict, probability_dict = getPartsDict(folderPath)
    #pinata_uploader = PinataUploader()
    for key in parts_dict.keys():
        print(f'{key}: ', len(parts_dict[key]))


    random_seed = 50
    rng = [np.random.default_rng() for i in range(len(parts_dict.keys()))]

    traits = []
    TOTAL_TRAITS = 10
    for i in range(TOTAL_TRAITS):
         newTrait = generateTrait()
         traits.append(newTrait)

    print("Are traits unique?")
    print(allUnique(traits))

    traits = addTokenID(traits)
    trait_keys = list(traits[0].keys())
    trait_keys = ['Hintergrund', 'Boden', 'Body',  'Hat', 'Line', 'Shadow',  'Light', 'Gesicht', 'Waffe', 'Accessoires', 'Rahmen', 'tokenID']
    print(trait_keys)
    metadata_json = {}
    all_metadata_json = {}
    # statistics = traitStatistics(traits, trait_keys[:-1], parts_dict)
    for i,trait in enumerate(traits):
         img, filename = generateImage(trait, trait_keys, folderPath)
    #     ipfs_hash = pinata_uploader.upload_image(img, filename, pinata_key_ending=f'{int(i/TOTAL_TRAITS * 3)+1}')
    #     trait['ipfs_hash'] = ipfs_hash
    #     saveMetadata(trait, trait_keys)
    #     if ipfs_hash is None:
    #         print("ERROR!")
    #         sys.exit()
    #     meta_key = 'id_' + str(trait['tokenID'])
    #     meta_key = str(trait['tokenID'])
    #     all_metadata_json[meta_key] = trait
    #     print(f"Created image {i+1}")
    #     with open('all_metadata.json', 'w') as fp:
    #         json.dump(all_metadata_json, fp)

    # response = pinata_uploader.upload_directory('../Masks/JSON-FILES', pinata_key_ending=f'{3}')
